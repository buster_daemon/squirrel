unit AboutGPL;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls;

type

  { TGPLForm }

  TGPLForm = class(TForm)
    GPLMemo: TMemo;
    GPLLogo: TImage;
    LeftSplitter: TSplitter;
    RightSplitter: TSplitter;
    BottomSplitter: TSplitter;
    TopSplitter: TSplitter;
  private

  public

  end;

var
  GPLForm: TGPLForm;

implementation

{$R *.lfm}

end.

