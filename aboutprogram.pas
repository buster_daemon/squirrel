unit AboutProgram;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls;

type

  { TAboutPgrForm }

  TAboutPgrForm = class(TForm)
    DevTitle: TLabel;
    DevList: TMemo;
    Logo: TImage;
    Label1: TLabel;
    Panel1: TPanel;
    PgrSubTitle: TLabel;
    PgrTitle: TLabel;
    BottomSplitter: TSplitter;
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  AboutPgrForm: TAboutPgrForm;

resourcestring
  AuthTitle = 'Author:';
  ContribTitle = 'Contributor:';
  Au1 = 'Buster Daemon - Coding';
  Cb1 = 'Growley - Ideas, Logo';

implementation

{$R *.lfm}

{ TAboutPgrForm }

procedure TAboutPgrForm.FormCreate(Sender: TObject);
begin
  DevList.Append(AuthTitle);
  DevList.Append(Au1);
  DevList.Append('');
  Devlist.Append(ContribTitle);
  DevList.Append(Cb1);
end;

end.

